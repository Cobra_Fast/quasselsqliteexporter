﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuasselSqliteExporter.DataObjects
{
	internal class PrimaryKeyAttribute : Attribute
	{ }

	internal class Backlog
	{
		[PrimaryKey]
		public long messageid { get; set; }

		public long time { get; set; }

		public long bufferid { get; set; }

		public long type { get; set; }

		public long flags { get; set; }

		public long senderid { get; set; }

		public string senderprefixes { get; set; } = string.Empty;

		public string message { get; set; } = string.Empty;
	}

	internal class Buffer
	{
		[PrimaryKey]
		public long bufferid { get; set; }

		public long usierid { get; set; }

		//public long groupid { get; set; }

		public long networkid { get; set; }

		public string buffername { get; set; } = string.Empty;

		public string buffercname { get; set; } = string.Empty;

		public long buffertype { get; set; }

		public long lastmsgid { get; set; }

		public long lastseenmsgid { get; set; }

		public long bufferactivity { get; set; }

		public long highlightcount { get; set; }

		public string key { get; set; } = string.Empty;

		public long joined { get; set; }

		public string cipher { get; set; } = string.Empty;
	}

	internal class Network
	{
		[PrimaryKey]
		public long networkid { get; set; }

		public long userid { get; set; }

		public string networkname { get; set; } = string.Empty;
	}

	internal class Sender
	{
		[PrimaryKey]
		public long senderid { get; set; }

		public string sender { get; set; } = string.Empty;

		public string realname { get; set; } = string.Empty;

		public string avatarurl { get; set; } = string.Empty;
	}
}
