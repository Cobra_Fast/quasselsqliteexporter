﻿using Microsoft.Data.Sqlite;
using QuasselSqliteExporter;

const string DatabaseLocation = @"C:\Users\Cobra_Fast\Documents\quassel-storage.sqlite";
const string OutputDirectory = @"C:\Users\Cobra_Fast\Documents\quassel-logs\";


using var connection = new SqliteConnection(@"Data Source=" + DatabaseLocation);
connection.Open();

IDictionary<long, QuasselSqliteExporter.DataObjects.Buffer> buffers;
using (var result = connection.Query("SELECT * FROM buffer"))
{
	buffers = result.ReadMapAll<long, QuasselSqliteExporter.DataObjects.Buffer>();
}

IDictionary<long, QuasselSqliteExporter.DataObjects.Network> networks;
using (var result = connection.Query("SELECT * FROM network"))
{
	networks = result.ReadMapAll<long, QuasselSqliteExporter.DataObjects.Network>();
}

IDictionary<long, QuasselSqliteExporter.DataObjects.Sender> senders;
using (var result = connection.Query("SELECT * FROM sender"))
{
	senders = result.ReadMapAll<long, QuasselSqliteExporter.DataObjects.Sender>();
}


foreach (var buffer in buffers.Values)
{
	long lines = 0;
	var fileName = $"{networks[buffer.networkid].networkname}-{buffer.buffername}.txt";
	var filePath = Path.Combine(OutputDirectory, fileName);
	Console.WriteLine($"Exporting {filePath}");

	using var writer = File.OpenWrite(filePath);
	using var textWriter = new StreamWriter(writer);
	textWriter.NewLine = "\n";

	using var result = connection.Query(
		"SELECT * FROM backlog WHERE bufferid = $bufferid ORDER BY time ASC", ("$bufferid", buffer.bufferid));

	while (result.Read())
	{
		var message = result.MapData<QuasselSqliteExporter.DataObjects.Backlog>();
		var line = $"{message.time.ToDateTime()} <{senders[message.senderid].sender}> {message.message}";
		textWriter.WriteLine(line);
		lines++;
	}

	textWriter.Close();
	
	Console.WriteLine($"{lines} lines exported.");
}
