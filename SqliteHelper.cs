﻿using FastMember;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace QuasselSqliteExporter
{
	internal static class SqliteHelper
	{
		public static SqliteDataReader Query(this SqliteConnection connection,
			string query,
			params (string name, object value)[] parameters)
		{
			var command = connection.CreateCommand();
			command.CommandText = query;

			foreach (var parameter in parameters)
				command.Parameters.AddWithValue(parameter.name, parameter.value);

			return command.ExecuteReader();
		}

		public static void MapData<T>(this SqliteDataReader reader, ref T target) where T : class
		{
			var oma = TypeAccessor.Create(target.GetType());
			var phs = oma.GetMembers().Select(mp => mp.Name).ToHashSet();

			for (int i = 0; i < reader.FieldCount; i++)
			{
				var fieldName = reader.GetName(i);
				if (phs.Contains(fieldName))
				{
					try
					{
						oma[target, fieldName] = reader.IsDBNull(i) ? null : reader.GetValue(i);
					}
					catch (Exception ex)
					{
						Console.WriteLine($"{ex.Message} while trying to map {fieldName} to {target.GetType().Name}.");
					}
				}
			}
		}

		public static T MapData<T>(this SqliteDataReader reader) where T : class, new()
		{
			var obj = new T();
			reader.MapData(ref obj);
			return obj;
		}

		public static IDictionary<TKey, TObject> ReadMapAll<TKey, TObject>(this SqliteDataReader reader)
			where TKey : struct
			where TObject : class, new()
		{
			var result = new Dictionary<TKey, TObject>();
			var acc = TypeAccessor.Create(typeof(TObject));
			var pk = acc.GetMembers()
				.First(m => Attribute.IsDefined(m.GetMemberInfo()!, typeof(DataObjects.PrimaryKeyAttribute)));

			while (reader.Read())
			{
				var obj = reader.MapData<TObject>();
				var key = (TKey)acc[obj, pk.Name];
				result.Add(key, obj);
			}

			return result;
		}

		public static MemberInfo? GetMemberInfo(this Member member)
		{
			var field = member.GetType().GetField("member", BindingFlags.Instance | BindingFlags.NonPublic);
			return (MemberInfo?)field?.GetValue(member);
		}

		public static DateTime ToDateTime(this long timestamp)
		{
			return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
				.AddMilliseconds(timestamp);
		}
	}
}
